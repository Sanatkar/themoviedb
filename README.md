THEMovieDB Project

This project has been written based on popular actors api of themoviedb.org

This project architecture is Based on MVVM design pattern, Which I have seprated my business logic from presentation.
We have used no stroy boards. Xib files used for cells only.

We have a tab bar controller managing our two Root view controllers : 
HomeViewController And WatchListViewController. Other view controllers are being pushed.

Each view controller has its own view model as same as cells.
I used common view models any where I could. for example MovieViewViewModel

We have used protocols anywhere we need! For example on watch list management I have implemented the business protocol based! 
The better testability was the result!

Unit testing was also added for network layer, models serialization and some business processes 
like add or remove from watch list and image downloading process and etc.

For image downloading I have implemented my own protocol But for the first time I have faced with the lack of sdwebimage
.... I felt in love with it more than before.

For paging in HomeViewController I have used UITableViewDataSourcePrefetching . It can be implemented in several user experiences.
I have selected the UX you can see in home page.

Movie detail and actor detail view controllers are fully implemented by pure code. 
Search is enable in HomeViewController navigation bar using UISearchController

