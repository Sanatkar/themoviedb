//
//  PersonTests.swift
//  TheMovieDBTests
//
//  Created by Hooman Sanatkar on 2019-06-04.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import XCTest
@testable import TheMovieDB

class PersonTests: XCTestCase {
    
    func checkPersonSerialization(){
        let path = Bundle(for: MovieTests.self).path(forResource: "SamplePerson", ofType: "json")
        
        do {
            let movieData = try? Data(contentsOf: URL(fileURLWithPath: path!))
            let person = try JSONDecoder().decode(Person.self, from: movieData!)
            XCTAssertEqual(person.id, 87167)
        }catch {
            XCTFail("Could not serialize person model")
        }
    }
    
}
