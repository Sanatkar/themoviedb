//
//  MovieTests.swift
//  TheMovieDBTests
//
//  Created by Hooman Sanatkar on 2019-06-04.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation

import XCTest
@testable import TheMovieDB

class MovieTests: XCTestCase {
    
    func checkMovieSerialization(){
        let path = Bundle(for: MovieTests.self).path(forResource: "SampleMovie", ofType: "json")
        
        do {
            let movieData = try? Data(contentsOf: URL(fileURLWithPath: path!))
            let movie = try JSONDecoder().decode(Movie.self, from: movieData!)
             XCTAssertEqual(movie.id, 49018)
        }catch {
            XCTFail("Could not serialize movie model")
        }
    }
    
}
