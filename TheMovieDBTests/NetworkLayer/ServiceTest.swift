//
//  ServiceTest.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-06-04.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import XCTest
@testable import TheMovieDB

class ServiceTest: XCTestCase {
    
    var sut:Service!
    override func setUp() {
        super.setUp()
        sut = Service()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testResponseSerialization() {
        let promise = expectation(description: "Response serialized to expected model")
        
        sut.getPopularActors(page: 1, completion: { (netResponse) in
            switch netResponse{
            case .success:
                promise.fulfill()
            case .failure(let message):
                XCTFail("Response failed Error:\(message)")
            @unknown default:
                 XCTFail("Something wrong happens ")
            }
        })
        wait(for: [promise], timeout: 20)
    }
    
    func testWrongUrl() {
        let promise = expectation(description: "Response serialized to expected model")
        
        sut.getPopularActors(page: 1, completion: { (netResponse) in
            switch netResponse{
            case .success:
                promise.fulfill()
            case .failure(let message):
                XCTFail("Response failed Error:\(message)")
            @unknown default:
                XCTFail("Something wrong happens ")
            }
        })
        wait(for: [promise], timeout: 20)
    }
}
