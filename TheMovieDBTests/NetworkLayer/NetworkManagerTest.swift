//
//  NetworkManagerTest.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-06-04.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import XCTest
@testable import TheMovieDB

class NetworkManagerTest: XCTestCase {
    
    var sut:NetworkManager!
    override func setUp() {
        super.setUp()
        sut = NetworkManager()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testRequestBuilder() {
        do {
            let endPoint = EndPoint.popular(page: 4)
            let request = try sut.buildRequest(from: endPoint)
            let expectedURL = "https://api.themoviedb.org/3/person/popular?page=4&api_key=6bfeff91dadeb64721adc1769f57706d"
            
            XCTAssertNotNil(request,"request for end point can not be built")
            XCTAssertEqual(request.url!.absoluteString.sorted(), expectedURL.sorted())
        }catch {
            XCTFail("Could not build request for popular endpoint")
        }
    }
    
    func testValidCallWithStatusCode200() {
        let endPoint = EndPoint.popular(page:1)
        let promise = expectation(description: "Status code: 200 ... 299")
        
        sut.request(endPoint: endPoint) { data,response,error in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if 200 ... 299 ~= statusCode  {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        wait(for: [promise], timeout: 20)
    }
}
