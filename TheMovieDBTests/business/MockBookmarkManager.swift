//
//  MockBookmarkManager.swift
//  TheMovieDBTests
//
//  Created by Hooman Sanatkar on 2019-06-04.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation

class MockBookmarker:Bookmarkable{
    
    var movie:Movie
    init() {
        let path = Bundle(for: MovieTests.self).path(forResource: "SampleMovie", ofType: "json")
        let movieData = try? Data(contentsOf: URL(fileURLWithPath: path!))
        let movie = try! JSONDecoder().decode(Movie.self, from: movieData!)
        self.movie = movie
    }
}
