//
//  BusinessTests.swift
//  TheMovieDBTests
//
//  Created by Hooman Sanatkar on 2019-06-04.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import XCTest
@testable import TheMovieDB

class BusinessTests: XCTestCase {
    
    func testAddToToBookmark(){
        let mockBookmarker = MockBookmarker.init()
        mockBookmarker.bookmarkMovie(movie: mockBookmarker.movie)
        XCTAssertTrue(mockBookmarker.checkIfMovieIsBookmarked(movie:mockBookmarker.movie),"Movie can not be added to watch list")
    }
    
    func testRemoveFromBookmark(){
        let mockBookmarker = MockBookmarker.init()
        mockBookmarker.bookmarkMovie(movie: mockBookmarker.movie)
        mockBookmarker.unBookmarkMovie(movie: mockBookmarker.movie)
       XCTAssertFalse(mockBookmarker.checkIfMovieIsBookmarked(movie:mockBookmarker.movie),"Movie can not be removed from watch list")
    }
    
    func testImageDownloading(){
         let promise = expectation(description: "Image Downloaded Successfully")
          let mockImageDownloader = MockImageDownloader.init()
        mockImageDownloader.downloadAsyncImage(imagePosfix:mockImageDownloader.movie.poster_path ?? "", imageView:.none) { data in
            if data != nil {
                promise.fulfill()
            }
            else{
                XCTFail("Can not download image data")
            }
        }
        wait(for: [promise], timeout: 10)
    }
    
}
