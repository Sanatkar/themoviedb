//
//  NetworkManager.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

public typealias NetworkManagerCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()


class NetworkManager{
    
    private var task_PopularActores: URLSessionTask?
    private var imageViewToTask:[UIImageView:URLSessionTask] = [:]
    static let apiKey = "6bfeff91dadeb64721adc1769f57706d"
    
    func request(endPoint:EndPoint, completion: @escaping NetworkManagerCompletion) {
       
        self.task_PopularActores?.cancel()
        let session = URLSession.shared
        do {
            let request = try self.buildRequest(from: endPoint)
            task_PopularActores = session.dataTask(with: request, completionHandler: { data, response, error in
                completion(data, response, error)
            })
        }catch {
            completion(nil, nil, error)
        }
        self.task_PopularActores?.resume()
    }
    
    func requestImageDownlaod(imagePath:String,imageView:UIImageView?, completion: @escaping NetworkManagerCompletion) {
        
        var task_ImageDownloader:URLSessionTask?
        if imageView != nil {
            task_ImageDownloader = self.imageViewToTask[imageView!]
        }
        
        task_ImageDownloader?.cancel()
        let session = URLSession.shared
        let request = URLRequest(url: URL.init(string: imagePath)!,
                                 cachePolicy:.useProtocolCachePolicy,
                                 timeoutInterval: 40.0)
        task_ImageDownloader = session.dataTask(with: request, completionHandler: { data, response, error in
            completion(data, response, error)
        })
        task_ImageDownloader?.resume()
        guard (task_ImageDownloader != nil) , imageView != nil else {return}
        self.imageViewToTask[imageView!] = task_ImageDownloader
    }
    
    func buildRequest(from endPoint:EndPoint) throws -> URLRequest {
        
        let urlAdress = self.urlFromEndPoint(endPoint: endPoint)
        var request = URLRequest(url: URL.init(string: urlAdress)!,
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 20.0)
        request.httpMethod = endPoint.httpMethod
        return request
    }
    
    fileprivate func urlFromEndPoint(endPoint:EndPoint) -> String{
        return endPoint.baseURL.absoluteString + endPoint.path + "&api_key=\(NetworkManager.apiKey)"
    }
    
}

