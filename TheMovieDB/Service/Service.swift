//
//  Service.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

class Service{
    static let MovieAPIKey = "6bfeff91dadeb64721adc1769f57706d"
    static let BaseURL = "https://api.themoviedb.org"
    let imagesCache = NSCache<NSString, NSData>()
    let netwrokManager = NetworkManager()
    
    func getPopularActors(page:Int,completion: @escaping (NetworkResponse<PersonsListResponse>) -> Void){
        
        netwrokManager.request(endPoint:.popular(page: page)) { data, response, error in
            
            if error != nil { completion(NetworkResponse.failure(message:error!.localizedDescription))
            }
            
            if let response = response as? HTTPURLResponse ,  200 ... 299 ~= response.statusCode {
                guard let responseData = data else {
                    completion(NetworkResponse.failure(message:"No Data Recieved"))
                    return
                }
                do {
                    let personsList = try JSONDecoder().decode(PersonsListResponse.self, from: responseData)
                    completion(NetworkResponse.success(value: personsList))
                }catch let error { completion(NetworkResponse.failure(message:error.localizedDescription))
                }
                
            }
        }
        
    }
    
    func downloadImage(imageAddress:String,imageView:UIImageView?,completion:@escaping(Data?)->Void){
       
        if  imageView != nil, let imageData = self.imagesCache.object(forKey:imageAddress as NSString) {
            completion((imageData as Data))
        }
        else{
            netwrokManager.requestImageDownlaod(imagePath: imageAddress,imageView: imageView){
                data, response, error in
                if error != nil {
                    completion(nil)
                }
                else{
                    if let data = data {
                        if let url = response?.url {
                            self.imagesCache.setObject(data as NSData, forKey: url.absoluteString as NSString)
                        }
                    }
                    completion(data)
                }
               
            }
        }
    }
}
