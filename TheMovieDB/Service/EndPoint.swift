//
//  EndPoint.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation

protocol EndPointSpecs {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod:String { get }
}

enum EndPoint{
    case popular(page:Int)
}

extension EndPoint:EndPointSpecs{
    var baseURL: URL{
        return URL.init(string: GlobalConstants.Strings.serverAddress)!
    }
    
    var path: String{
        switch self {
        case .popular(let page):
            return "/person/popular?page=\(page)"
        default:
            return ""
        }
    }
    
    var httpMethod: String{
       
        return "get"
        //Switch case is needed for more than one endpoints
    }
    
    
}
