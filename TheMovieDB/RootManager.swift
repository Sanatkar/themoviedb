//
//  RootManager.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-06-03.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

final class RootManager {
    
    weak var window: UIWindow!
    let service = Service()
    init(window: UIWindow) {
        self.window = window
    }
    
    func presentMainScreen() {
        guard let window = window else { return }
        window.rootViewController = makeTabBarViewController()
        window.makeKeyAndVisible()
    }
    
    private func makeTabBarViewController() -> UIViewController {
        let tabBarViewController = UITabBarController()
        tabBarViewController.viewControllers = [
            makeHomeViewController(),
            makeWatchListViewController()
        ]
        
        
        return tabBarViewController
    }
    
    private func makeHomeViewController() -> UIViewController {
       
        let homeViewController: HomeViewController = {
            let vc = HomeViewController()
            vc.title = "Home"
            vc.tabBarItem.image = UIImage(named: "home-Unselected")
            vc.tabBarItem.selectedImage = UIImage(named: "home-Selected")
            return vc
        }()
        
        let navigationController = UINavigationController.init(rootViewController: homeViewController)
        navigationController.tabBarItem = homeViewController.tabBarItem
        navigationController.title = homeViewController.title
        
        let homeViewModel = HomeViewModel.init(service: self.service)
        homeViewController.homeViewModel = homeViewModel
    
        homeViewModel.onSelectMovie = { [weak navigationController] movie in
            let movieInfoViewController = MovieInfoViewController()
            movieInfoViewController.viewModel = MovieViewViewModel.init(movie: movie, service: self.service)
            navigationController?.pushViewController(movieInfoViewController, animated: true)
        }
        
        homeViewModel.onSelectActor = { [weak navigationController] actor in
            let actorInfoViewController = ActorInfoViewController()
            actorInfoViewController.actor = actor
            actorInfoViewController.service = self.service
            navigationController?.pushViewController(actorInfoViewController, animated: true)
        }
        
        return navigationController
    }
    
    
    private func makeWatchListViewController() -> UIViewController {
        let watchListViewController: WatchListViewController = {
            let vc = WatchListViewController()
            vc.title = "Watch List"
            vc.tabBarItem.image = UIImage(named: "watchList-Unselected")
            vc.tabBarItem.selectedImage = UIImage(named: "watchList-Selected")
            return vc
        }()
        
        let navigationController = UINavigationController.init(rootViewController: watchListViewController)
        navigationController.tabBarItem = watchListViewController.tabBarItem
        navigationController.title = watchListViewController.title
        
        let watchListViewModel = WatchListViewModel.init(service: self.service)
        watchListViewController.viewModel = watchListViewModel
        
        watchListViewModel.onSelectMovie = { [weak navigationController] movie in
            let movieInfoViewController = MovieInfoViewController()
            movieInfoViewController.viewModel = MovieViewViewModel.init(movie: movie, service: self.service)
            navigationController?.pushViewController(movieInfoViewController, animated: true)
        }
        
       
        return navigationController
        
    }
    
}

