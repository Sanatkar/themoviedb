//
//  Dynamic.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-31.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
class Dynamic<T> {
    typealias Listener = (T) -> ()
    var listener: Listener?
    
    func bind(_ listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ v: T) {
        value = v
    }
}
