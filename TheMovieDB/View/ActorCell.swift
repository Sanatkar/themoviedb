//
//  ActorCell.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-31.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

class ActorCell: UITableViewCell {
    
    @IBOutlet weak var label_ActorName:UILabel!
    @IBOutlet weak var imageView_Actor:UIImageView!
    @IBOutlet weak var collectionView_Movies:UICollectionView!
    @IBOutlet weak var activityIndicator_LoadingData:UIActivityIndicatorView!
   
    func configure(with viewModel: ActorCellViewModel?) {
        self.viewModel = viewModel
    }
    
    var viewModel:ActorCellViewModel? {
        didSet{
            addObservers()
            fillData()
        }
    }
    
    
    override func awakeFromNib() {
        
    self.selectionStyle = .none
    self.collectionView_Movies?.register(UINib(nibName:MovieCell.cellIdentifier(), bundle: Bundle.main), forCellWithReuseIdentifier: MovieCell.cellIdentifier())
      
        if let layout = self.collectionView_Movies.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        self.collectionView_Movies.delegate = self
        self.collectionView_Movies.dataSource = self
        
    }
    
    func addObservers(){
        viewModel?.imageData.bind{ [weak self] value in
            DispatchQueue.main.async {
                if value != nil {
                    self?.imageView_Actor.image = UIImage.init(data: value!)
                }
                else{
                    self?.imageView_Actor.image = nil
                }
            }
            
        }
    }
    
    func fillData(){
        self.imageView_Actor.image = nil
        self.label_ActorName.text = self.viewModel?.actorName ?? ""
        self.viewModel?.downloadImage(imageView: self.imageView_Actor)
        self.collectionView_Movies.collectionViewLayout.invalidateLayout()
        self.collectionView_Movies.reloadData()
        if self.viewModel != nil {
            self.activityIndicator_LoadingData.stopAnimating()
        }
        else{
            self.activityIndicator_LoadingData.startAnimating()
        }
    }
}

extension ActorCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel?.numberOfSections() ?? 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.numberOfItemsInSection() ?? 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCell.cellIdentifier(), for: indexPath) as! MovieCell
        cell.movieCelLModel = self.viewModel?.viewModelForMovieCellAtIndex(index: indexPath.row)
        cell.contentView.isUserInteractionEnabled = false 
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellMovieItemWidth = UIScreen.main.bounds.size.width/2.5
        let cellMovieItemHeight = (cellMovieItemWidth * (900/600)) + 100
        return CGSize.init(width: cellMovieItemWidth, height:cellMovieItemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        guard let movie = self.viewModel?.knownMovies[indexPath.row] , let viewModel = self.viewModel else{
            return
        }
        
        viewModel.onSelectMovie!(movie)
    }
}
