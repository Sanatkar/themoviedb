//
//  MovieCell.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-31.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

class MovieCell:UICollectionViewCell{
    
    @IBOutlet weak var label_Title:UILabel?
    @IBOutlet weak var imageView_Poster:UIImageView!
    @IBOutlet weak var btn_Bookmark:UIButton!
    
    override func prepareForReuse() {
        self.imageView_Poster.image = nil
        self.label_Title?.text = ""
    }
    
    var movieCelLModel:MovieViewViewModel?{
        didSet{
            if movieCelLModel != nil {
                addObservers()
                configWithData()
            }
            else{
                configWithNone()
            }
            
        }
    }
    
    func addObservers(){
        self.movieCelLModel?.isBookmarked.bind{[weak self] value in
            if value {
                self?.btn_Bookmark.setImage(UIImage.init(named: "bookmark-on"), for: .normal)
            }
            else{
                self?.btn_Bookmark.setImage(UIImage.init(named: "bookmark-off"), for: .normal)
            }
        }
        
        self.movieCelLModel?.imageData.bind{ [weak self] value in
            DispatchQueue.main.async {
                if value != nil {
                    self?.imageView_Poster.image = UIImage.init(data: value!)
                }
                else{
                    self?.imageView_Poster.image = nil
                }
            }
            
        }
    }
    
    @IBAction func bookmarkMovie(){
        self.movieCelLModel?.flipBookmark()
    }
    
    func configWithData(){
        
        self.imageView_Poster.image = UIImage.init(named:"moviePlaceHolder")
        self.label_Title?.text = self.movieCelLModel!.movie!.original_title ?? "No Name"
        self.label_Title?.font = UIFont.systemFont(ofSize: (self.label_Title?.font.pointSize)!)
        self.movieCelLModel?.downloadImage(imageView:self.imageView_Poster)
    }
    
    func configWithNone(){
        self.imageView_Poster.image = UIImage.init(named:"moviePlaceHolder")
        self.label_Title?.text = "ASAS ASALSKJA"
        self.label_Title?.font = UIFont.init(name:GlobalConstants.Strings.fontName_Block, size: (self.label_Title?.font.pointSize)!)
    }
    
    
    
    
    
}
