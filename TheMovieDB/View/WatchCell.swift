//
//  WatchCell.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-31.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

class WatchCell: UITableViewCell {
    
    @IBOutlet weak var label_Title:UILabel!
    @IBOutlet weak var imageView_Poster:UIImageView!
    
    var WatchCellViewModel:MovieViewViewModel?{
        didSet{
            addObservers()
            fillData()
        }
    }
    
    func addObservers(){
        self.WatchCellViewModel?.imageData.bind{ [weak self] value in
            DispatchQueue.main.async {
                if value != nil {
                    self?.imageView_Poster.image = UIImage.init(data: value!)
                }
                else{
                    self?.imageView_Poster.image = nil
                }
            }
            
        }
    }
    
    func fillData(){
        self.label_Title.text = self.WatchCellViewModel!.movie!.title ?? "No Name"
        self.WatchCellViewModel?.downloadImage(imageView:self.imageView_Poster)
    }
    
    
}
