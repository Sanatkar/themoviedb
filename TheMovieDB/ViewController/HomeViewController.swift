//
//  HomeViewController.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController:UITableViewController,AlertPrenentor{
    
    let cellItemsWidth = UIScreen.main.bounds.size.width/2.5
    let cellItemsHeight = ((UIScreen.main.bounds.size.width/2.5) * (900/600)) + 100
    var homeViewModel:HomeViewModel!
    var activityIndicator:UIActivityIndicatorView?
    let searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addObservers()
        self.setupTableView()
        self.setupSearchController()
        self.setupNavigationController()
        self.homeViewModel.fetchActors()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let visibleRows = self.tableView.indexPathsForVisibleRows{
             self.tableView.reloadRows(at:visibleRows, with: .none)
        }
    }
    
    func setupTableView(){
        tableView.register(UINib(nibName:ActorCell.cellIdentifier(), bundle: Bundle.main), forCellReuseIdentifier: ActorCell.cellIdentifier())
        tableView.prefetchDataSource = self
        tableView.estimatedRowHeight = cellItemsHeight + 60
    }
    
    func setupSearchController(){
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Actors"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func setupNavigationController(){
        self.activityIndicator = UIActivityIndicatorView.init(style:.gray)
        self.activityIndicator?.hidesWhenStopped = true
        self.activityIndicator?.stopAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: self.activityIndicator!)
    }
    
    func addObservers(){
        
        homeViewModel.isLoadingContent.bind{ [weak self] value in
            DispatchQueue.main.async {
                if value {
                    self?.activityIndicator?.startAnimating()
                }
                else{
                     self?.activityIndicator?.stopAnimating()
                }
            }
        }
        
        homeViewModel.isFiltering.bind{ [weak self] value in
            DispatchQueue.main.async {
               self?.tableView.reloadData()
            }
           
        }
        
        homeViewModel.error.bind{ [weak self] value in
            guard value.count > 0 else {
                return
            }
            var actions = [UIAlertAction.init(title:"Ok", style: .cancel, handler: nil)]
            if self?.homeViewModel.currentActorsCount == 0 {
                let retryAction =  UIAlertAction(title:"Retry", style: UIAlertAction.Style.default, handler: { action in
                    self?.homeViewModel.fetchActors()
                })
                actions.append(retryAction)
            }
            self?.presentAlert(with:"Error", message: value, actions:actions)
        }
        
        homeViewModel.indexPathsToReload.bind{ [weak self] value in
            guard value.count > 0 else {
                return
            }
           
            DispatchQueue.main.async {
                let indexPathsToReload = self?.visibleIndexPathsToReload(intersecting: self?.homeViewModel.indexPathsToReload.value ?? []) ?? []
                
                if self?.tableView.visibleCells.count == 0 {
                    self?.tableView.reloadData()
                }
                else{
                    if indexPathsToReload.count > 0 {
                        self?.tableView.reloadRows(at: indexPathsToReload, with: .none)
                    }
                }
            }
        }
    }
    
    
}

extension HomeViewController{
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.homeViewModel.isFiltering.value  {
           return self.homeViewModel.filteredActorsCount
        }
        
         return self.homeViewModel.totalActorsCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:ActorCell.cellIdentifier(), for: indexPath) as! ActorCell
        if indexPath.row >= homeViewModel.currentActorsCount{
            cell.configure(with: .none)
        }
        else{
            if self.homeViewModel.isFiltering.value  {
                cell.configure(with:self.homeViewModel.viewModelForActorAtIndex(index: indexPath.row))
            }
            cell.configure(with:self.homeViewModel.viewModelForActorAtIndex(index: indexPath.row))
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return cellItemsHeight + 60
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         self.homeViewModel.onSelectActor!(self.homeViewModel.actor(at: indexPath.row))
    }
}

extension HomeViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
        if (indexPaths.filter{
            $0.row >= homeViewModel.currentActorsCount
        }).count > 0 && !homeViewModel.isFiltering.value{
            homeViewModel.fetchActors()
        }
    }
    
}

extension HomeViewController{
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        return indexPath.row >= homeViewModel.currentActorsCount
    }
    
    func visibleIndexPathsToReload(intersecting indexPaths: [IndexPath]) -> [IndexPath] {
        let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows ?? []
        let indexPathsIntersection = Set(indexPathsForVisibleRows).intersection(indexPaths)
        return Array(indexPathsIntersection)
    }
}

extension HomeViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        self.homeViewModel.filterContentForSearchText(searchBar.text!)
    }
}
