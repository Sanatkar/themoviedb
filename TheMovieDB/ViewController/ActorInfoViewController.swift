//
//  ActorInfoViewController.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

class ActorInfoViewController:UIViewController,ImageDownloadbale{
 
    var service: Service?
    var scrollV: UIScrollView!
    var imageView_Poster: UIImageView!
    var label_Title: UILabel!
    var label_Score: UILabel!
    var btn_Bookmark: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSubviews()
        self.applyConstraints()
        fillData()
    }
    
    var actor:Person!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func fillData(){
        
        self.imageView_Poster.image = nil
        self.label_Title?.text = self.actor.name ?? "No name"
        self.label_Score.text =
        "Users Popularity : \(self.actor?.popularity ?? 0.0)"
        
        self.downloadAsyncImage(imagePosfix: self.actor.profile_path ?? "", imageView: self.imageView_Poster) {data in
            guard let data = data else {return}
            self.imageView_Poster.image = UIImage.init(data: data)
        }
    }
    
}

extension ActorInfoViewController{
    
    func addSubviews(){
        self.scrollV = UIScrollView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight))
        self.scrollV.backgroundColor = UIColor.white
        self.view.addSubview(self.scrollV)
        
        self.imageView_Poster = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: screenWidth * 1.2))
        self.imageView_Poster.backgroundColor = UIColor.init(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
        self.imageView_Poster.contentMode = .scaleAspectFill
        self.imageView_Poster.clipsToBounds = true
        self.scrollV.addSubview(self.imageView_Poster)
        
       
        self.label_Title = UILabel.init(frame: CGRect.init(x: 10, y: self.imageView_Poster.frame.origin.y + self.imageView_Poster.frame.height + 5, width: screenWidth - 20, height: 30))
        self.label_Title.numberOfLines = 0
        self.label_Title.font = UIFont.boldSystemFont(ofSize: 22.0)
        self.scrollV.addSubview(self.label_Title)
        
        self.label_Score = UILabel.init(frame: CGRect.init(x: 10, y: self.label_Title.frame.origin.y + self.label_Title.frame.height + 5, width: 200, height: 40))
        self.label_Score.numberOfLines = 0
        self.label_Score.font = UIFont.boldSystemFont(ofSize: 18.0)
        self.scrollV.addSubview(self.label_Score)
        
    }
    
    func applyConstraints(){
        
        self.scrollV.translatesAutoresizingMaskIntoConstraints = false
        self.imageView_Poster.translatesAutoresizingMaskIntoConstraints = false
        self.label_Title.translatesAutoresizingMaskIntoConstraints = false
        self.label_Score.translatesAutoresizingMaskIntoConstraints = false
        
        let scrollView_TopConstraint = self.scrollV.topAnchor.constraint(equalTo: self.view.topAnchor)
        let scrollView_RightConstraint = self.scrollV.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        let scrollView_LeftConstraint = self.scrollV.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
        let scrollView_Height = self.scrollV!.heightAnchor.constraint(equalToConstant: screenHeight)
        view.addConstraints([scrollView_TopConstraint, scrollView_RightConstraint, scrollView_LeftConstraint,scrollView_Height])
        
        
        let imageView_TopConstraint = self.imageView_Poster.topAnchor.constraint(equalTo: scrollV.topAnchor)
        let imageView_LeftConstraint = self.imageView_Poster.leadingAnchor.constraint(equalTo: scrollV.leadingAnchor)
        let imageView_Width = self.imageView_Poster.widthAnchor.constraint(equalToConstant: screenWidth)
        let imageView_Height = self.imageView_Poster!.heightAnchor.constraint(equalToConstant: screenWidth * 1.2)
        self.scrollV.addConstraints([imageView_TopConstraint, imageView_LeftConstraint,imageView_Width,imageView_Height])
        
        
        let titleLabel_TopConstraint = self.label_Title.topAnchor.constraint(equalTo: imageView_Poster.bottomAnchor)
        let titleLabel_LeftConstraint = self.label_Title.leadingAnchor.constraint(equalTo: scrollV.leadingAnchor, constant: 10)
        let titleLabel_Width = self.label_Title.widthAnchor.constraint(equalToConstant: screenWidth - 20)
        let titleLabel_Height = self.label_Title!.heightAnchor.constraint(greaterThanOrEqualToConstant: 30)
        self.scrollV.addConstraints([titleLabel_TopConstraint, titleLabel_LeftConstraint,titleLabel_Width,titleLabel_Height])
        
        
        let scoreLabel_TopConstraint = self.label_Score.topAnchor.constraint(equalTo: label_Title.bottomAnchor, constant: 10)
        let scoreLabel_Bottomonstraint = self.label_Score.bottomAnchor.constraint(equalTo: scrollV.bottomAnchor)
        let scoreLabel_LeftConstraint = self.label_Score.leadingAnchor.constraint(equalTo: scrollV.leadingAnchor, constant: 10)
        let scoreLabel_Width = self.label_Score.widthAnchor.constraint(equalToConstant: screenWidth - 20)
        let scoreLabel_Height = self.label_Score!.heightAnchor.constraint(greaterThanOrEqualToConstant: 40)
        self.scrollV.addConstraints([scoreLabel_TopConstraint, scoreLabel_Bottomonstraint,scoreLabel_LeftConstraint,scoreLabel_Width,scoreLabel_Height])
        
        
    }
    
}


