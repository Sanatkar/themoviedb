//
//  WatchListViewController.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

protocol WatchListViewControllerProtocol {
    func movieAddedToWatchList(movie:Movie)
    func moviewRemovedFromWatchList(movie:Movie)
}


class WatchListViewController:UITableViewController,Bookmarkable{
    
    var viewModel:WatchListViewModel!
    var service:Service!
    override func viewDidLoad() {
        super.viewDidLoad()
         tableView.register(UINib(nibName:WatchCell.cellIdentifier(), bundle: Bundle.main), forCellReuseIdentifier: WatchCell.cellIdentifier())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:WatchCell.cellIdentifier(), for: indexPath) as! WatchCell
        cell.WatchCellViewModel = self.viewModel.viewModelForCell(at: indexPath.row)
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            unBookmarkMovie(movie: self.viewModel.bookmark(at: indexPath.row))
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = self.viewModel.bookmark(at: indexPath.row)
        self.viewModel.onSelectMovie!(movie)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

