//
//  Person.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation

struct PersonsListResponse:Codable {
    let page:Int
    let total_results:Int
    let total_pages:Int
    let results:[Person]
}

struct Person:Codable {
    
    let popularity: Float?
    let id: Int?
    let profile_path: String?
    let name: String?
    let known_for: [Movie]?
    let adult: Bool?
}
