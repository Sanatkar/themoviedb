//
//  Movie.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation

struct Movie:Codable {
    
     let vote_average: Float?
     let vote_count: Int?
     let id: Int?
     let video: Bool?
     let media_type: String?
     let title: String?
     let popularity: Float?
     let poster_path: String?
     let original_language: String?
     let original_title: String?
     let genre_ids: [Int]?
     let backdrop_path: String?
     let adult: Bool?
     let overview: String?
     let release_date: String?
     let origin_country:[String]?
    
}
