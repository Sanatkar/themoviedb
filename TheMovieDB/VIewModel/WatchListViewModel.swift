//
//  WatchListViewModel.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-06-03.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit
class WatchListViewModel:Bookmarkable {
    
    
    var service:Service?
    var onSelectMovie: ((Movie) -> Void)?
    
    init(service:Service){
        self.service = service
    }
    
    func viewModelForCell(at index:Int) -> MovieViewViewModel{
        return MovieViewViewModel.init(movie: self.bookmark(at: index), service: self.service!)
    }
    
    func numberOfSections() -> Int{
        return 1
    }
    
    func numberOfRows() -> Int {
        return self.getAllBookmarks().count
    }
    
    func bookmark(at index:Int) -> Movie{
        return self.getAllBookmarks()[index]
    }
    
    
}
