//
//  ActorCellViewModel.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-31.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

class ActorCellViewModel:ImageDownloadbale{
    
    var imageData: Dynamic<Data?> = Dynamic(nil)
    var service: Service?
    var actorName:String
    var actorPoster:String
    var knownMovies:[Movie]
    
    init(actorName:String,actorPoster:String,knownMovies:[Movie],service:Service){
        self.service = service
        self.actorName = actorName
        self.actorPoster = actorPoster
        self.knownMovies = knownMovies
        
    }
    
    func downloadImage(imageView:UIImageView){
        downloadAsyncImage(imagePosfix:actorPoster,imageView:imageView){ (data) in
            self.imageData.value = data
        }
    }
    
    var onSelectMovie: ((Movie) -> Void)?
    
    func viewModelForMovieCellAtIndex(index:Int) -> MovieViewViewModel? {
        if index >= self.knownMovies.count {
            return .none
        }
        return MovieViewViewModel.init(movie: self.knownMovies[index],service: self.service!)
       
    }
    
    func numberOfSections() -> Int{
        return 1
    }
    
    func numberOfItemsInSection() -> Int{
        return self.knownMovies.count
    }

    
}
