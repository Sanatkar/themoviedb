//
//  HomeViewModel.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-31.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

class HomeViewModel{
    
    private var actors:[Person] = []
    private var filteredActors = [Person]()
    private var currentPage = 1
    private var totalResults = 0
    var isLoadingContent: Dynamic<Bool> = Dynamic(false)
    var isFiltering: Dynamic<Bool> = Dynamic(false)
    var error:Dynamic<String> = Dynamic("")
    var indexPathsToReload:Dynamic<[IndexPath]> = Dynamic([])
    let service:Service?
    
    init(service:Service?) {
        self.service = service
    }
    
    var onSelectMovie: ((Movie) -> Void)?
    var onSelectActor: ((Person) -> Void)?
    
    var currentActorsCount:Int {
        return self.actors.count
    }
    
    var filteredActorsCount:Int{
        return self.filteredActors.count
    }
    
    var totalActorsCount:Int{
        return self.totalResults
    }
    
    func actor(at index: Int) -> Person {
        return actors[index]
    }
    
    func filteredActor(at index:Int) -> Person{
        return filteredActors[index]
    }
    
    func viewModelForActorAtIndex(index:Int) -> ActorCellViewModel{
        let actor = self.isFiltering.value ? self.filteredActor(at: index) : self.actor(at: index)
        let actorCellViewModel = ActorCellViewModel.init(actorName: actor.name ?? "", actorPoster: actor.profile_path ?? "", knownMovies: actor.known_for ?? [],service: self.service!)
        actorCellViewModel.onSelectMovie = self.onSelectMovie
        return actorCellViewModel
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        guard searchText.count > 0 else {
            self.isFiltering.value = false
            return
        }
        
        self.isFiltering.value = true
        filteredActors = self.actors.filter{
            ($0.name?.contains(searchText))!
        }
        self.indexPathsToReload.value = (0..<self.filteredActorsCount).map { IndexPath(row: $0, section: 0) }
    }
    
    func fetchActors(){
        
        guard !isLoadingContent.value else {
            return
        }
        
        self.isLoadingContent.value = true
        self.service?.getPopularActors(page: currentPage, completion: { (netResponse) in
            switch netResponse {
            case .success(let value):
                self.isLoadingContent.value = false
                self.currentPage += 1
                self.totalResults = value.total_results
                self.actors.append(contentsOf: value.results)
                self.indexPathsToReload.value = self.indexPathsToReload(from: value.results)
            case .failure(let message):
                self.isLoadingContent.value = false
                self.error.value = message
             default:
                break
            }
        })
    }
    
    private func indexPathsToReload(from newActors: [Person]) -> [IndexPath] {
        let startIndex = actors.count - newActors.count
        let endIndex = startIndex + newActors.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
    
}
