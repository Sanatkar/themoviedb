//
//  Bookmarkable.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-06-01.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation

protocol Bookmarkable {
    func bookmarkMovie(movie:Movie)
    func unBookmarkMovie(movie:Movie)
    func checkIfMovieIsBookmarked(movie:Movie) -> Bool
    func getAllBookmarks() -> [Movie]
}

extension Bookmarkable{
    func bookmarkMovie(movie:Movie) {
        let encodedData = try? JSONEncoder().encode(movie)
        guard var watchListOnUserDefaults = UserDefaults.standard.value(forKey: GlobalConstants.Strings.key_WatchList) as? [Data] else{
            UserDefaults.standard.set([encodedData], forKey: GlobalConstants.Strings.key_WatchList)
            return
        }
       
        watchListOnUserDefaults.append(encodedData!)
        UserDefaults.standard.set(watchListOnUserDefaults, forKey: GlobalConstants.Strings.key_WatchList)
    }
    
    func unBookmarkMovie(movie:Movie){
        guard var watchListOnUserDefaults = UserDefaults.standard.value(forKey: GlobalConstants.Strings.key_WatchList) as? [Data] else {
            return
        }
        watchListOnUserDefaults = watchListOnUserDefaults.filter{
            return (try? JSONDecoder().decode(Movie.self, from: $0) )!.id != movie.id
        }
        
        UserDefaults.standard.set(watchListOnUserDefaults, forKey: GlobalConstants.Strings.key_WatchList)
    }
    
    func checkIfMovieIsBookmarked(movie:Movie) -> Bool{
        guard let watchListOnUserDefaults = UserDefaults.standard.value(forKey: GlobalConstants.Strings.key_WatchList) as? [Data] else{
            return false
        }
        return watchListOnUserDefaults.filter{
            return (try? JSONDecoder().decode(Movie.self, from: $0) )!.id == movie.id
        }.count > 0
    }
    
    func getAllBookmarks() -> [Movie]{
        return (UserDefaults.standard.value(forKey:GlobalConstants.Strings.key_WatchList) as? [Data] ?? []).map{
            try! JSONDecoder().decode(Movie.self, from: $0)
        }
    }
}
