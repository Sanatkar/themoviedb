//
//  ImageDownloadable.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-06-02.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

protocol ImageDownloadbale {
    var service:Service? { get }
    func downloadAsyncImage(imagePosfix:String,imageView:UIImageView?,completion:@escaping(Data?)->Void)
    
}

extension ImageDownloadbale{
    func downloadAsyncImage(imagePosfix:String,imageView:UIImageView?,completion:@escaping(Data?)->Void){
        let path = "\(GlobalConstants.Strings.imagesServerAddress)\(imagePosfix)"
        service?.downloadImage(imageAddress: path,imageView: imageView) { (data) in
            completion(data)
        }
    }
}

