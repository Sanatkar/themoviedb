//
//  MovieViewViewModel.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-06-02.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

class MovieViewViewModel:Bookmarkable,ImageDownloadbale{
    var imageData: Dynamic<Data?> = Dynamic(nil)
    var service: Service?
    
    
    var isBookmarked:Dynamic<Bool> = Dynamic(false)
    var movie:Movie?
    
    init (movie:Movie?,service:Service?){
        self.movie = movie
        self.service = service
        self.checkBookmark()
        
    }
    
    func downloadImage(imageView:UIImageView){
        downloadAsyncImage(imagePosfix:self.movie?.poster_path ?? "",imageView:imageView){ (data) in
            self.imageData.value = data
        }
    }
    
    func checkBookmark(){
         if let movie = movie {
            self.isBookmarked.value = checkIfMovieIsBookmarked(movie: movie)
        }
    }
    
    func flipBookmark(){
        if self.isBookmarked.value {
            self.unBookmarkMovie(movie:self.movie!)
            self.isBookmarked.value = false
        }
        else{
            self.bookmarkMovie(movie:self.movie!)
            self.isBookmarked.value = true
        }
    }
    
}
