//
//  AppDelegate.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var rootManager:RootManager?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        let rootManager = RootManager(window: window)
        rootManager.presentMainScreen()
        
        
        self.rootManager = rootManager
        self.window = window
        
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
     
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}

