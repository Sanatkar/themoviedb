//
//  GlobalConstants.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-30.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation

struct GlobalConstants {
   
    struct Strings{
        static let serverAddress = "https://api.themoviedb.org/3"
        static let imagesServerAddress = "https://image.tmdb.org/t/p/w185_and_h278_bestv2"
        static let key_WatchList = "WatchList"
        static let fontName_Block = "BLOKK"
    }
    
    struct DateFormatters {
        static let simpleDateFormatter: DateFormatter = {
            var dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return dateFormatter
        }()
    }
}

enum NetworkResponse<T: Codable> {
    case success(value: T)
    case failure(message: String)
}

