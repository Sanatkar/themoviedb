//
//  Cell+Extension.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-06-01.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

 extension UITableViewCell {
    public static func cellIdentifier() -> String {
        return String(describing: self)
    }
}

extension UICollectionViewCell {
    public static func cellIdentifier() -> String {
        return String(describing: self)
    }
}
