//
//  AlertPrenentor.swift
//  TheMovieDB
//
//  Created by Hooman Sanatkar on 2019-05-31.
//  Copyright © 2019 CafeBazar. All rights reserved.
//

import Foundation
import UIKit

protocol AlertPrenentor {
    func presentAlert(with title: String, message: String, actions: [UIAlertAction]?)
}

extension AlertPrenentor where Self: UIViewController {
    func presentAlert(with title: String, message: String, actions: [UIAlertAction]? = nil) {
        guard presentedViewController == nil else {
            return
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions?.forEach { action in
            alertController.addAction(action)
        }
        present(alertController, animated: true)
    }
}

